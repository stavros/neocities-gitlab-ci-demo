Neocities GitLab CI demo
========================

This is a demo repository, showing how you can use GitLab CI to deploy a site to [Neocities](https://neocities.org/).

Steps:

1. Get a supporter account on Neocities (this is required for WebDAV support).
2. Copy the `.gitlab-ci.yml` file from this repository to yours.
2. Edit `.gitlab-ci.yml` and add your username and your site's name.
3. Put your HTML into the `pages` directory.
4. Add a masked, protected variable called `NEOCITIES_PASSWORD` containing your actual Neocities password to your CI.
5. Push, and that's it, you should see your site deployed shortly.

The CI configuration uses [rclone](https://rclone.org/) to copy your files to Neocities.
You can customize it to add another stage to generate the HTML using a static site generator, just make sure you
register that output as an artifact so the "deploy" stage can see it.

Have fun!
